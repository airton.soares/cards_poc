defmodule CardsPocWeb.Router do
  use CardsPocWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", CardsPocWeb do
    pipe_through :api
  end
end
