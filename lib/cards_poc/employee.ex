defmodule CardsPoc.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "employees" do
    field :cpf, :string
    field :name, :string
    field :employer_id, :binary_id
    field :physical_card_id, :binary_id
    field :digital_card, :binary_id

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:name, :cpf])
    |> validate_required([:name, :cpf])
  end
end
