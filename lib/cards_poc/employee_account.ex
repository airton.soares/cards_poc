defmodule CardsPoc.EmployeeAccount do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "employee_accounts" do
    field :active, :boolean, default: false
    field :balance, :float
    field :name, :string
    field :type, Ecto.Enum, values: [:food, :meal, :culture]
    field :employee_id, :binary_id

    timestamps()
  end

  @doc false
  def changeset(employee_account, attrs) do
    employee_account
    |> cast(attrs, [:name, :type, :balance, :active])
    |> validate_required([:name, :type, :balance, :active])
  end
end
