defmodule CardsPoc.Repo do
  use Ecto.Repo,
    otp_app: :cards_poc,
    adapter: Ecto.Adapters.Postgres
end
