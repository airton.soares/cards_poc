defmodule CardsPoc.DigitalCard do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "digital_cards" do
    field :active, :boolean, default: false
    field :limit, :float

    timestamps()
  end

  @doc false
  def changeset(digital_card, attrs) do
    digital_card
    |> cast(attrs, [:active, :limit])
    |> validate_required([:active, :limit])
  end
end
