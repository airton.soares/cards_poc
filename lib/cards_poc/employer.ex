defmodule CardsPoc.Employer do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "employers" do
    field :active, :boolean, default: false
    field :balance, :float
    field :cnpj, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(employer, attrs) do
    employer
    |> cast(attrs, [:name, :cnpj, :active, :balance])
    |> validate_required([:name, :cnpj, :active, :balance])
  end
end
