defmodule CardsPoc.PhysicalCard do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "physical_cards" do
    field :active, :boolean, default: false
    field :limit, :float

    timestamps()
  end

  @doc false
  def changeset(physical_card, attrs) do
    physical_card
    |> cast(attrs, [:active, :limit])
    |> validate_required([:active, :limit])
  end
end
