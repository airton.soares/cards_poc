defmodule CardsPoc.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :cpf, :string
      add :employer_id, references(:employers, on_delete: :nothing, type: :binary_id)
      add :physical_card_id, references(:physical_cards, on_delete: :nothing, type: :binary_id)
      add :digital_card, references(:digital_cards, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:employees, [:employer_id])
    create index(:employees, [:physical_card_id])
    create index(:employees, [:digital_card])
  end
end
