defmodule CardsPoc.Repo.Migrations.CreateDigitalCards do
  use Ecto.Migration

  def change do
    create table(:digital_cards, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :active, :boolean, default: false, null: false
      add :limit, :float

      timestamps()
    end
  end
end
