defmodule CardsPoc.Repo.Migrations.CreateEmployeeAccounts do
  use Ecto.Migration

  def change do
    create table(:employee_accounts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :type, :string
      add :balance, :float
      add :active, :boolean, default: false, null: false
      add :employee_id, references(:employees, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:employee_accounts, [:employee_id])
  end
end
