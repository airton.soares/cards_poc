defmodule CardsPoc.Repo.Migrations.CreateEmployers do
  use Ecto.Migration

  def change do
    create table(:employers, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :cnpj, :string
      add :active, :boolean, default: false, null: false
      add :balance, :float

      timestamps()
    end
  end
end
